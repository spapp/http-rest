<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.04.03.
 */

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

if (!defined('APPLICATION_ENV')) {
    if (getenv('APPLICATION_ENV')) {
        define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
    } else {
        define('APPLICATION_ENV', 'production');
    }
}

if ('production' !== APPLICATION_ENV) {
    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
    ini_set('log_errors', true);
    error_reporting(E_ALL | E_STRICT | E_DEPRECATED);
}

require(APPLICATION_PATH . '/library/Autoloader.php');

Autoloader::getInstance()
          ->addIncludePath(
                  array(
                          APPLICATION_PATH . '/library',
                          APPLICATION_PATH . '/application/controller',
                  )
          )
          ->register();

require(APPLICATION_PATH . '/application/Application.php');

new Application(APPLICATION_PATH . '/application/config/' . APPLICATION_ENV . '.php', APPLICATION_ENV);












































