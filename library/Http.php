<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.25.
 */

/**
 * Class Http
 */
class Http {
    const EOL         = "\r\n";
    const PROTOCOL_11 = 'HTTP/1.1';

    const HEADER_ACCEPT              = 'Accept';
    const HEADER_ACCEPT_ENCODING     = 'Accept-Encoding';
    const HEADER_ACCEPT_LANGUAGE     = 'Accept-Language';
    const HEADER_CACHE_CONTROL       = 'Cache-Control';
    const HEADER_CONNECTION          = 'Connection';
    const HEADER_CONTENT_TYPE        = 'Content-Type';
    const HEADER_CONTENT_DISPOSITION = 'Content-Disposition';
    const HEADER_HOST                = 'Host';
    const HEADER_IF_MODIFIED_SINCE   = 'If-Modified-Since';
    const HEADER_IF_NONE_MATCH       = 'If-None-Match';
    const HEADER_USER_AGENT          = 'User-Agent';

    const CONTENT_TYPE_TEXT            = 'text/plain';
    const CONTENT_TYPE_HTML            = 'text/html';
    const CONTENT_TYPE_XML             = 'application/xml';
    const CONTENT_TYPE_JSON            = 'application/json';
    const CONTENT_TYPE_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const CONTENT_TYPE_FORM_DATA       = 'multipart/form-data';

    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_GET     = 'GET';
    const METHOD_HEAD    = 'HEAD';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_DELETE  = 'DELETE';
    const METHOD_TRACE   = 'TRACE';
    const METHOD_CONNECT = 'CONNECT';

    protected static $responseMessages = array(
            0   => 'Unknown',
        // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',

        // Successful 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',

        // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',

        // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',

        // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
    );

    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     *
     *
     * @param number $code
     *
     * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10
     * @static
     * @return string
     */
    public static function getResponseCodeAsString($code) {
        if (!isset(self::$responseMessages[$code])) {
            $code = 0;
        }

        return self::$responseMessages[$code];
    }
}
