<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Traits
 * @since      2015.04.13.
 */

namespace Traits;

/**
 * Trait Data
 */
trait Data {
    /**
     * Data container
     *
     * @var array
     */
    protected $data = array();

    /**
     * Returns all data as an array
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Sets all data
     *
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data = null) {
        if (null === $data) {
            $data = array();
        }

        $this->data = $data;

        return $this;
    }

    /**
     * Returns a data value
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($name, $default = null) {
        if ($this->has($name)) {
            $default = $this->data[$name];
        }

        return $default;
    }

    /**
     * Removes all data values
     *
     * @return $this
     */
    public function clear() {
        $this->data = array();

        return $this;
    }

    /**
     * Delete a data
     *
     * @param string $name
     *
     * @return $this
     */
    public function delete($name) {
        if ($this->has($name)) {
            unset($this->data[$name]);
        }

        return $this;
    }

    /**
     * Returns TRUE if the data is exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name) {
        return array_key_exists($name, (array)$this->data);
    }

    /**
     * Sets a data value
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function set($name, $value) {
        $this->data[$name] = $value;

        return $this;
    }
}
