<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Traits
 * @since      2015.03.27.
 */

namespace Traits;

/**
 * Trait Options
 */
trait Options {
    /**
     * Options container
     *
     * @var array
     */
    protected $options = array();

    /**
     * Returns all options
     *
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * Sets all options
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options = null) {
        if (null === $options) {
            $options = array();
        }

        $this->options = $options;

        return $this;
    }

    /**
     * Returns an option value
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getOption($name, $default = null) {
        if ($this->hasOption($name)) {
            $default = $this->options[$name];
        }

        return $default;
    }

    /**
     * Returns TRUE if the option is exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasOption($name) {
        return array_key_exists($name, (array)$this->options);
    }

    /**
     * Sets an option value
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function setOption($name, $value) {
        $this->options[$name] = $value;

        return $this;
    }
}
