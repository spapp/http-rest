<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.23.
 */

use Config\Exception;

/**
 * Class Config
 */
class Config {
    /**
     * @var array
     */
    protected $data = array();

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct(array $data = null) {
        if (is_array($data)) {
            $this->merge($data);
        }
    }

    /**
     * Merge data to the config recursively
     *
     * @param array $data
     *
     * @return $this
     */
    protected function merge(array $data) {
        $this->data = array_merge_recursive($this->data, $data);

        return $this;
    }

    /**
     * Returns config as an array
     *
     * @return array
     */
    public function toArray() {
        return $this->data;
    }

    /**
     * Returns TRUE if the config value is exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name) {
        return array_key_exists($name, $this->data);
    }

    /**
     * Returns a config value
     *
     * If it is not exists then returns the default value
     *
     * @param string $name
     * @param mixed  $default (optional)
     *
     * @return Config|mixed
     */
    public function get($name, $default = null) {
        if ($this->has($name)) {
            $return = $this->data[$name];
        } else {
            $return = $default;
        }

        if (is_array($return)) {
            $return = new self($return);
        }

        return $return;
    }

    /**
     * Sets up a config value
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function set($name, $value) {
        $this->data[$name] = $value;

        return $this;
    }

    /**
     * Loads config from a file
     *
     * @param string $fileName
     *
     * @return $this
     * @throws Exception
     */
    public function load($fileName) {
        if (file_exists($fileName)) {
            $extension = pathinfo($fileName, PATHINFO_EXTENSION);

            switch ($extension) {
                case 'php':
                    $data = include($fileName);
                    break;
                case 'ini':
                    $data = parse_ini_file($fileName, true);
                    break;
                default:
                    throw new Exception('Not supported file type: ' . $extension);
            }

            $this->merge($data);
        } else {
            throw new Exception('File not exists: ' . $fileName);
        }

        return $this;
    }

    /**
     * Returns a config class
     *
     * @param string|array $data
     *
     * @static
     * @return Config
     * @throws Exception
     */
    public static function factory($data) {
        $instance = new self();

        if ('string' === gettype($data)) {
            $instance->load($data);
        } elseif (is_array($data)) {
            $instance->merge($data);
        } else {
            throw new Exception('Not supported data type');
        }

        return $instance;
    }
}
