<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content
 * @since      2015.03.25.
 */

namespace Http\Content;

use Http;

/**
 * Class Http\Content\Parser
 */
class Parser {
    const NS_PARSER = '\\Http\\Content\\Parser\\';

    protected static $types = array(
            Http::CONTENT_TYPE_TEXT            => 'Text',
            Http::CONTENT_TYPE_HTML            => 'Html',
            Http::CONTENT_TYPE_XML             => 'Xml',
            Http::CONTENT_TYPE_JSON            => 'Json',
            Http::CONTENT_TYPE_FORM_URLENCODED => 'Form',
            Http::CONTENT_TYPE_FORM_DATA       => 'Formdata'
    );

    /**
     * Constructor
     */
    protected function __construct() {
    }

    /**
     * Factory method
     *
     * Returns a content parser class
     *
     * @param string|array $type
     * @param array        $options
     *
     * @static
     * @return ParserAbstract
     */
    public static function factory($type, array $options = null) {
        if (is_array($type)) {
            $options = $type;
            $type    = $options['type'];
        }

        $class = self::NS_PARSER . self::$types[$type];

        return new $class($options);
    }
}
