<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content\Parser
 * @since      2015.03.26.
 */

namespace Http\Content\Parser;

use Http;

/**
 * Class Json
 */
class Json extends ParserAbstract {
    /**
     * @inheritdoc
     *
     * @param array $value
     *
     * @return string
     */
    public function stringify(array $value) {
        return json_encode($value);
    }

    /**
     * @inheritdoc
     *
     * @param string $text json string
     *
     * @return array
     */
    public function parse($text) {
        return json_decode($text, true);
    }
}
