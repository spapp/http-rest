<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content\Parser
 * @since      2015.03.25.
 */

namespace Http\Content\Parser;

use Http;

/**
 * Class Text
 */
class Text extends ParserAbstract {
    /**
     * @inheritdoc
     *
     * @param array $value
     *
     * @return string
     */
    public function stringify(array $value) {
        return implode(Http::EOL, $value);
    }

    /**
     * @inheritdoc
     *
     * @param string $text
     *
     * @return array
     */
    public function parse($text) {
        if (!$text) {
            return array();
        }

        return explode(Http::EOL, $text);
    }
}
