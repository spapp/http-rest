<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content\Parser
 * @since      2015.04.02.
 */

namespace Http\Content\Parser;

use Http;
use Traits\Data;

/**
 * Class Html
 */
class Html extends Text {
    use Data {
        Data::getData as getParams;
        Data::setData as setParams;
    }

    const TEMPLATE_SUFFIX = 'phtml';

    /**
     * @param string $template relative path of template
     *
     * @return string
     */
    public function render($template) {
        ob_start();

        include($this->getTemplatePath($template));

        $output = ob_get_contents();

        ob_clean();

        return $output;
    }

    /**
     * @inheritdoc
     *
     * @param array $value
     *
     * @return string
     */
    public function stringify(array $value) {
        $this->setParams($value);

        return $this->render($this->getOption('template'));
    }

    /**
     * @inheritdoc
     *
     * @param string $text
     *
     * @return array
     */
    public function parse($text) {
        return array();
    }

    /**
     * @return mixed|string
     */
    protected function getTemplatePath($template) {
        $suffix = $this->getOption('templateSuffix', self::TEMPLATE_SUFFIX);

        if (!preg_match('~\.' . $suffix . '$~', $template)) {
            $template .= '.' . $suffix;
        }

        return $this->getOption('templatePath') . DIRECTORY_SEPARATOR . $template;
    }
}
