<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content\Parser
 * @since      2015.03.26.
 */

namespace Http\Content\Parser;

use Http;
use Http\Content\Part as ContentPart;

/**
 * Class Formdata
 */
class Formdata extends ParserAbstract {
    const PREFIX_BOUNDARY = '--';

    protected static $index = 0;

    /**
     * @inheritdoc
     *
     * @param array $value
     *
     * @return string
     */
    public function stringify(array $data) {
        $return = array();

        $boundary = $this->getBoundary();

        foreach ($data as $name => $value) {
            array_push(
                    $return,
                    self::PREFIX_BOUNDARY . $boundary,
                    Http::HEADER_CONTENT_DISPOSITION . ': form-data; name="' . $name . '"',
                    ''
            );

            switch (gettype($value)) {
                case 'array':
                    array_push($return, json_encode($value));
                    break;
                default:
                    array_push($return, $value);
            }
        }

        if(count($return)>0) {
            array_push($return, self::PREFIX_BOUNDARY . $boundary . self::PREFIX_BOUNDARY);
        }

        return implode(Http::EOL, $return);
    }

    /**
     * @inheritdoc
     *
     * @param string $text
     *
     * @return array
     */
    public function parse($text) {
        $data   = explode(self::PREFIX_BOUNDARY . $this->getBoundary(), trim($text));
        $params = array();

        foreach ($data as $line) {
            $line = trim($line);

            if (!$line or '--' === $line) {
                continue;
            }

            $part        = new ContentPart($line);
            $disposition = current($part->getHeader()->get(Http::HEADER_CONTENT_DISPOSITION, '', true));
            $contentType = $part->getHeader()->get(Http::HEADER_CONTENT_TYPE, '', true);
            $content     = $part->getContent();

            if (is_array($contentType)) {
                $contentType = key($contentType);
            } elseif (preg_match('~^\{~', $content)) {
                $contentType = Http::CONTENT_TYPE_JSON;
            }

            if (Http::CONTENT_TYPE_JSON === $contentType) {
                $content = json_decode($content, true);
            }

            $params[$disposition['name']] = $content;
        }

        return $params;
    }

    protected function getBoundary() {
        $boundary = $this->getOption('boundary');

        if (!$boundary) {
            $boundary = md5(mktime() . '-' . (++self::$index));
        }

        return $boundary;
    }
}























