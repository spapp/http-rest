<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content\Parser
 * @since      2015.03.25.
 */

namespace Http\Content\Parser;

use Traits\Options;
use Http\Content;

/**
 * Class ParserAbstract
 */
abstract class ParserAbstract {
    use Options;

    /**
     * @param array $options
     */
    public function __construct(array $options = null) {
        $this->setOptions($options);
    }

    /**
     * The stringify method converts an array to a string.
     *
     * @param array $value
     *
     * @return string
     */
    public abstract function stringify(array $value);

    /**
     * The parse method parses a string as array.
     *
     * @param string $text
     *
     * @return array
     */
    public abstract function parse($text);
}
