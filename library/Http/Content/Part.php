<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content
 * @since      2015.03.31.
 */

namespace Http\Content;

use Http;
use Http\Header;

/**
 * Class Part
 */
class Part {
    /**
     * @var Header
     */
    protected $header = null;
    /**
     * @var string
     */
    protected $content = null;

    /**
     * Constructor
     *
     * @param string $text Optional
     */
    public function __construct($text = null) {
        if ($text) {
            $this->parse($text);
        }
    }

    /**
     * Parse a http content part
     *
     * @param string $text
     *
     * @return $this
     */
    public function parse($text) {
        $parts = preg_split('~\r?\n\r?\n~', $text, 2);

        $this->setHeader(rtrim($parts[0]));
        $this->setContent(ltrim($parts[1]));

        return $this;
    }

    /**
     * Returns headers of part
     *
     * @return Header|null
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * Sets header of part
     *
     * @param Header|string $header
     *
     * @return $this
     */
    public function setHeader($header) {
        if ('string' === gettype($header)) {
            $header = new Header($header);
        }

        $this->header = $header;

        return $this;
    }

    /**
     * Returns content of part
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Sets content of part
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Magice function
     *
     * @return string
     */
    public function __toString() {
        return $this->getHeader() . Http::EOL . Http::EOL . $this->getContent();
    }
}
