<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Content
 * @since      2015.04.01.
 */

namespace Http\Content;

/**
 * Class Exception
 */
class Exception extends \Exception {

}
