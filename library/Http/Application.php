<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.04.02.
 */

namespace Http;

use Config;

/**
 * Class Application
 */
class Application {
    /**
     * @var Config
     */
    protected $config = null;
    /**
     * @var Request
     */
    protected $request = null;
    /**
     * @var Response
     */
    protected $response = null;
    /**
     * @var bool
     */
    protected $isRun = false;
    /**
     * @var Router
     */
    protected $router = null;
    /**
     * @var Controller
     */
    protected $controller = null;
    /**
     * @var string
     */
    protected $environment = null;

    /**
     * Constructor
     *
     * @param Config|array|string $config
     * @param string              $environment
     */
    public function __construct($config, $environment) {
        $this->setEnvironment($environment);
        $this->setConfig($config);
    }

    /**
     * Returns the application environment
     *
     * @return null
     */
    public function getEnvironment() {
        return $this->environment;
    }

    /**
     * Sets the application environment
     *
     * @param $this $environment
     */
    public function setEnvironment($environment) {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Returns application config
     *
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Sets up application config
     *
     * @param Config|array|string $config
     *
     * @return $this
     * @throws Config\Exception
     */
    public function setConfig($config) {
        if (!($config instanceof Config)) {
            $config = Config::factory($config);
        }

        $this->config = $config;

        foreach ($this->config->get('php', array())->toArray() as $key => $value) {
            ini_set($key, $value);
        }

        return $this;
    }

    /**
     * Returns application request
     *
     * @return Request
     */
    public function getRequest() {
        if (null === $this->request) {
            $this->request = Request::getInstance();
        }

        return $this->request;
    }

    /**
     * Sets up application request
     *
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Returns application response
     *
     * @return Response
     */
    public function getResponse() {
        if (null === $this->response) {
            $this->response = Response::getInstance();
        }

        return $this->response;
    }

    /**
     * Sets up application response
     *
     * @param Response $response
     *
     * @return $this
     */
    public function setResponse(Response $response) {
        $this->response = $response;

        return $this;
    }

    /**
     * Run application
     *
     * @return $this
     */
    public function run() {
        if (false === $this->isRun) {
            $this->isRun = true;

            $this->setRouter($this->getConfig()->get('router', array()))
                 ->getRouter()
                 ->setRequest($this->getRequest())
                 ->route();

            // @todo error handling
            $this->getController()
                 ->setRequest($this->getRequest())
                 ->setResponse($this->getResponse());

            $this->getResponse()
                 ->getContent()
                 ->setOptions($this->getConfig()->get('content')->toArray());

            if (method_exists($this->getController(), $this->getRouter()->getActionMethodName())) {
                call_user_func(
                        array(
                                $this->getController(),
                                $this->getRouter()->getActionMethodName()
                        )
                );
            }
        }

        return $this;
    }

    /**
     * Returns current router
     *
     * @return Router
     */
    public function getRouter() {
        if (null === $this->router) {
            $this->setRouter();
        }

        return $this->router;
    }

    /**
     * Sets the current router object
     *
     * @param Router|array $options
     *
     * @return $this
     */
    public function setRouter($options = null) {
        if ($options instanceof Router) {
            $this->router = $options;
        } else {
            $this->router = new Router($options);
        }

        return $this;

    }

    /**
     * Returns current controller
     *
     * @return Controller
     */
    public function getController() {
        if (null === $this->controller) {
            $className = array();
            $config    = $this->getConfig()->get('controller', array());

            array_push(
                    $className,
                    $config->get('ns', ''),
                    $this->getRouter()->getControllerClassName()
            );

            $className = implode('\\', $className);

            $this->controller = new $className($config->toArray());
        }

        return $this->controller;
    }

    /**
     * Terminates the application
     *
     * Sends all http header and content
     *
     * @return $this
     */
    public function terminate() {
        if (false === $this->isRun) {
            $this->run();
        }

        $this->getResponse()->send();

        return $this;
    }

    /**
     * Destructor
     */
    public function __destruct() {
        $this->terminate();
    }
}
