<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.04.01.
 */

namespace Http;

use Traits\Options;

/**
 * Class Controller
 */
class Controller {

    use Options;

    /**
     * @var Request
     */
    protected $request = null;
    /**
     * @var Response
     */
    protected $response = null;

    /**
     * Constructor
     */
    public function __construct($options = null) {
        if ($options) {
            $this->setOptions($options);
        }
    }

    public function setTemplate($fileName) {
        $this->getResponse()->getContent()->setOption('template', $fileName);

        return $this;
    }

    /**
     * Set a param value
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function setParam($name, $value) {
        $this->getResponse()->getContent()->add($name, $value);

        return $this;
    }

    /**
     * Returns a param value
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getParam($name, $default = null) {
        return $this->getResponse()->getContent()->get($name, $default);
    }

    /**
     * Returns application request
     *
     * @return Request
     */
    public function getRequest() {
        if (null === $this->request) {
            $this->request = Request::getInstance();
        }

        return $this->request;
    }

    /**
     * Sets application request
     *
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Returns application response
     *
     * @return Response
     */
    public function getResponse() {
        if (null === $this->response) {
            $this->response = Response::getInstance();
        }

        return $this->response;
    }

    /**
     * Sets application response
     *
     * @param Response $response
     *
     * @return $this
     */
    public function setResponse(Response $response) {
        $this->response = $response;

        return $this;
    }
}
