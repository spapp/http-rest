<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.03.25.
 */

namespace Http;

use Http;
use Http\Content\Exception as ContentException;
use Http\Content\Parser;
use Http\Content\Parser\ParserAbstract;
use Traits\Data;
use Traits\Options;

/**
 * Class Content
 */
class Content {

    use Options;

    use Data {
        Data::set as add;
        Data::delete as remove;
    }

    /**
     * @var ParserAbstract
     */
    protected $parser = null;

    /**
     * Constructor
     *
     * @param string $data
     * @param array  $options
     */
    public function __construct($data, array $options = null) {
        if (is_array($data)) {
            $options = $data;
            $data    = null;
        } else if ('string' !== gettype($data)) {
            throw new ContentException('Not supported content type');
        }

        $this->setOptions($options);

        if (false === $this->hasOption('type')) {
            $this->setType(Http::CONTENT_TYPE_TEXT);
        }

        $this->setData($data);
    }

    /**
     * Sets data
     *
     * @param string $data
     *
     * @return $this
     */
    public function setData($data) {
        $this->data = $this->getParser()->parse($data);

        return $this;
    }

    /**
     * Returns a data parser
     *
     * @return ParserAbstract
     */
    public function getParser() {
        if (null === $this->parser) {
            $this->parser = Parser::factory($this->getOptions());
        }

        return $this->parser;
    }

    /**
     * Sets data type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type) {
        $this->parser = null;
        $this->setOption('type', $type);

        return $this;
    }

    /**
     * Returns data as a string
     *
     * @return string
     */
    public function toString() {
        return $this->getParser()->stringify($this->getData());
    }

    /**
     * Magice method
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }
}
