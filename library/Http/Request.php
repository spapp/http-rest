<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.03.24.
 */

namespace Http;

use Http;
use Http\Content;

/**
 * Class Request
 */
class Request {
    /**
     * @var Request
     * @static
     */
    protected static $instance = null;
    /**
     * @var Header
     */
    protected $header = null;
    /**
     * @var Content
     */
    protected $content = null;

    /**
     * @var array
     */
    protected $params = null;

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Returns request content
     *
     * @return Content
     */
    public function getContent() {
        if (null === $this->content) {
            $this->setContent(file_get_contents('php://input'));
        }

        return $this->content;
    }

    /**
     * Sets the request contentent
     *
     * @param string|Content $content
     *
     * @return $this
     */
    public function setContent($content) {
        if (!($content instanceof Content)) {
            $content = new Content($content, $this->getParsedContentType());
        }

        $this->content = $content;

        return $this;
    }

    /**
     * Returns request header
     *
     * @return Header
     */
    public function getHeader() {
        if (null === $this->header) {
            $headers = array();

            foreach ($_SERVER as $key => $value) {
                if (preg_match('~^HTTP_~', $key)) {
                    $headers[substr($key, 5)] = $value;
                }
            }

            $this->setHeader($headers);
        }

        return $this->header;
    }

    /**
     * Sets up request header
     *
     * @param mixed $header
     */
    public function setHeader($header) {
        if (!($header instanceof Header)) {
            $header = new Header($header);
        }

        $this->header = $header;
    }

    /**
     * Returns a $_SERVER variable
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getServer($name, $default = null) {
        $name = strtoupper($name);

        if (array_key_exists($name, $_SERVER)) {
            $default = $_SERVER[$name];
        }

        return $default;
    }

    /**
     * Returns TRUE if the request method is GET
     *
     * @return bool
     */
    public function isGet() {
        return (Http::METHOD_GET === $this->getMethod());
    }

    /**
     * Returns TRUE if the request method is POST
     *
     * @return bool
     */
    public function isPost() {
        return (Http::METHOD_POST === $this->getMethod());
    }

    /**
     * Returns TRUE if the request method is PUT
     *
     * @return bool
     */
    public function isPut() {
        return (Http::METHOD_PUT === $this->getMethod());
    }

    /**
     * Returns TRUE if the request method is DELETE
     *
     * @return bool
     */
    public function isDelete() {
        return (Http::METHOD_DELETE === $this->getMethod());
    }

    /**
     * Returns request method
     *
     * @return string
     */
    public function getMethod() {
        return strtoupper($this->getServer('REQUEST_METHOD'));
    }

    /**
     * Returns request URI
     *
     * @return string
     */
    public function getUri() {
        return $this->getServer('REQUEST_URI');
    }

    /**
     * Returns request URI path
     *
     * @todo cuts down to the file name
     * @return string
     */
    public function getPath() {
        return parse_url($this->getUri(), PHP_URL_PATH);
    }

    /**
     * Returns request uri query string as an array
     *
     * @return array
     */
    public function getQuery() {
        parse_str($this->getServer('QUERY_STRING'), $params);

        return $params;
    }

    /**
     * Returns all reques params
     *
     * @return array|mixed
     */
    public function getParams() {
        if (null === $this->params) {
            switch ($this->getMethod()) {
                case Http::METHOD_GET:
                    $this->params = $this->getQuery();
                    break;
                case Http::METHOD_POST:
                case Http::METHOD_PUT:
                case Http::METHOD_DELETE:
                    $this->params = $this->getContent()->getContent();
                    break;
                case Http::METHOD_OPTIONS:
                case Http::METHOD_HEAD:
                case Http::METHOD_TRACE:
                case Http::METHOD_CONNECT:
            }
        }

        return $this->params;
    }

    /**
     * Returns a request params
     *
     * If it is not exists then returns the default value
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return null
     */
    public function getParam($name, $default = null) {
        if ($this->hasParam($name)) {
            $default = $this->params[$name];
        }

        return $default;
    }

    /**
     * Returns TRUE if the params is exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasParam($name) {
        if (null === $this->params) {
            $this->getParams();
        }

        return (array_key_exists($name, $this->params));
    }

    /**
     * Returns the class instance
     *
     * @static
     * @return Request
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Parse content type HTTP header
     *
     * @return array
     */
    protected function getParsedContentType() {
        $contentType = $this->getHeader()->get(Http::HEADER_CONTENT_TYPE, null, true);

        $data         = $contentType[key($contentType)];
        $data['type'] = key($contentType);

        return $data;
    }
}
