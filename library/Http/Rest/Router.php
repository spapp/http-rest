<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Rest
 * @since      2015.04.01.
 */

namespace Http\Rest;

use Http\Router as HttpRouter;

/**
 * Class Router
 */
class Router extends HttpRouter {

    /**
     * @return $this
     */
    public function route() {
        parent::route();

        $this->actionName = strtolower($this->getRequest()->getMethod());

        return $this;
    }
}
