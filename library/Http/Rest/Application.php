<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Rest
 * @since      2015.03.24.
 */

namespace Http\Rest;

use Http\Application as HttpApplication;
use Http\Rest\Router as RestRouter;

/**
 * Class Rest\Application
 */
class Application extends HttpApplication {

    /**
     * Sets the current router object
     *
     * @param Router|array $options
     *
     * @return $this
     */
    public function setRouter($options = null) {
        if ($options instanceof Router) {
            $this->router = $options;
        } else {
            $this->router = new RestRouter($options);
        }

        return $this;

    }
}
