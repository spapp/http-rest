<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http\Rest
 * @since      2015.04.02.
 */

namespace Http\Rest;

use Http;
use Http\Controller as HttpController;

/**
 * Class Controller
 */
class Controller extends HttpController {
    /**
     * Sets the 'Method Not Allowed' (405) HTTP response
     *
     * @return void
     */
    protected final function doMethodNotAllowed() {
        $this->getResponse()
             ->getContent()
             ->clear()
             ->setType(Http::CONTENT_TYPE_TEXT)
             ->add('error', Http::getResponseCodeAsString(405));

        $this->getResponse()
             ->getHeader()
             ->setStatusCode(405);
    }

    public function optionsAction() {
        $this->doMethodNotAllowed();
    }

    public function getAction() {
        $this->doMethodNotAllowed();
    }

    public function headAction() {
        $this->doMethodNotAllowed();
    }

    public function postAction() {
        $this->doMethodNotAllowed();
    }

    public function putAction() {
        $this->doMethodNotAllowed();
    }

    public function deleteAction() {
        $this->doMethodNotAllowed();
    }

    public function traceAction() {
        $this->doMethodNotAllowed();
    }

    public function connectAction() {
        $this->doMethodNotAllowed();
    }
}
