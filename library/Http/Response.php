<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.03.31.
 */

namespace Http;

use Http\Content;

/**
 * Class Response
 */
class Response {
    /**
     * @var Response
     * @static
     */
    protected static $instance = null;
    /**
     * @var Header
     */
    protected $header = null;
    /**
     * @var Content
     */
    protected $content = null;

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Returns request content
     *
     * @return Content
     */
    public function getContent() {
        if (null === $this->content) {
            $this->setContent('');
        }

        return $this->content;
    }

    /**
     * Sets the request contentent
     *
     * @param string|Content $content
     *
     * @return $this
     */
    public function setContent($content) {
        if (!($content instanceof Content)) {
            $content = new Content($content);
        }

        $this->content = $content;

        return $this;
    }

    /**
     * Returns the response headers
     *
     * @return Header
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * Sets the response header
     *
     * @param Header $header
     *
     * @return $this
     */
    public function setHeader($header) {
        if (!($header instanceof Header)) {
            $header = new Header($header);
        }

        $this->header = $header;

        return $this;
    }

    /**
     * Send response header and content
     *
     * @return void
     */
    public function send() {
        if (!$this->getHeader()->hasSent()) {
            $this->getHeader()->send();
        }

        echo $this->getContent();
    }

    /**
     * Adds a response param
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function addParam($name, $value) {
        $this->getContent()->add($name, $value);

        return $this;
    }

    /**
     * Adds a response header
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function addHeader($name, $value) {
        $this->getHeader()->set($name, $value);

        return $this;
    }

    /**
     * Returns the class instance
     *
     * @static
     * @return Response
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
            self::$instance->setHeader(null);
        }

        return self::$instance;
    }
}
