<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.04.01.
 */

namespace Http;

use Traits\Options;

/**
 * Class Router
 */
class Router {

    use Options;

    const SUFFIX_ACTION_METHOD    = 'Action';
    const SUFFIX_CONTROLLER_CLASS = 'Controller';

    const DEFAULT_ACTION_NAME     = 'index';
    const DEFAULT_CONTROLLER_NAME = 'index';
    /**
     * @var string
     */
    protected $actionName = null;
    /**
     * @var string
     */
    protected $controllerName = null;
    /**
     * @var Request
     */
    protected $request = null;

    /**
     * Constructor
     */
    public function __construct($options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * @return $this
     */
    public function route() {
        if (false === $this->routeStatic()) {
            $this->routeUri();
        }

        if (null === $this->controllerName) {
            $this->controllerName = self::DEFAULT_CONTROLLER_NAME;
        }

        if (null === $this->actionName) {
            $this->actionName = self::DEFAULT_ACTION_NAME;
        }

        return $this;
    }

    /**
     * Returns the current controller full class name
     *
     * @return string
     */
    public function getControllerClassName() {
        return $this->prepareName($this->getControllerName() . ' ' . self::SUFFIX_CONTROLLER_CLASS);
    }

    /**
     * Returns the current controller name
     *
     * @return string
     */
    public function getControllerName() {
        if (null === $this->controllerName) {
            $this->route();
        }

        return ucfirst($this->controllerName);
    }

    /**
     * Returns the current controller action method full name
     *
     * @return string
     */
    public function getActionMethodName() {
        return lcfirst($this->prepareName($this->getActionName() . ' ' . self::SUFFIX_ACTION_METHOD));
    }

    /**
     * Returns the current controller action name
     *
     * @return string
     */
    public function getActionName() {
        if (null === $this->actionName) {
            $this->route();
        }

        return $this->actionName;
    }

    /**
     * Returns application request
     *
     * @return Request
     */
    public function getRequest() {
        if (null === $this->request) {
            $this->request = Request::getInstance();
        }

        return $this->request;
    }

    /**
     * Sets up application request
     *
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Route from uri
     *
     * @return void
     */
    protected function routeUri() {
        $path = trim($this->getRequest()->getPath(), '/');

        if ($path) {
            $pathArray = explode('/', $path);
        } else {
            $pathArray = array();
        }

        if (count($pathArray) > 0) {
            $this->controllerName = array_shift($pathArray);
        }

        if (count($pathArray) > 0) {
            $this->actionName = array_shift($pathArray);
        }
    }

    /**
     * Route from application config
     *
     * @return bool
     */
    protected function routeStatic() {
        $success = false;
        $path    = rtrim($this->getRequest()->getPath(), '/');
        $routes  = $this->getOption('route', array());

        foreach ($routes as $pattern => &$option) {
            if (preg_match($pattern, $path)) {
                if (array_key_exists('controller', $option)) {
                    $this->controllerName = $option['controller'];
                }

                if (array_key_exists('action', $option)) {
                    $this->actionName = $option['action'];
                }

                $success = true;

                break;
            }
        }

        return $success;
    }

    /**
     * Prepare method and class name
     *
     * @param string $name
     *
     * @return mixed
     */
    protected function prepareName($name) {
        $name = preg_replace('~[-_]~', ' ', $name);

        return str_replace(' ', '', ucwords($name));
    }
}
