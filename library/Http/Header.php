<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Http
 * @since      2015.03.24.
 */

namespace Http;

use Http;

/**
 * Class Header
 */
class Header {

    const EOL = "\r\n";

    /**
     * @var array
     */
    protected $headers = null;
    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Constructor
     *
     * @param mixed $headers
     */
    public function __construct($headers = null) {
        if ($headers) {
            $this->extract($headers);
        } else {
            $this->clear();
        }
    }

    /**
     * Sets the response status code
     *
     * @param int $statusCode
     *
     * @return $this
     */
    public function setStatusCode($statusCode) {
        $this->statusCode = (int)$statusCode;

        return $this;
    }

    /**
     * Returns a header value
     *
     * If it is not exists then returns the $default value
     *
     * @param string $name
     * @param string $default
     *
     * @return string
     */
    public function get($name, $default = '', $parsed = false) {
        if (true === $this->has($name)) {
            $default = $this->headers[strtolower($name)];
        }

        if (true === $parsed and $default) {
            $default = self::parseValue($default);
        }

        return $default;
    }

    /**
     * Sets up a header value
     *
     * @param string $name
     * @param string $value
     *
     * @return $this
     */
    public function set($name, $value) {
        $this->headers[strtolower($name)] = $value;

        return $this;
    }

    /**
     * Returns TRUE if the header is exists
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name) {
        return array_key_exists(strtolower($name), (array)$this->headers);
    }

    /**
     * Send all HTTP headers
     *
     * @return $this
     */
    public function send() {
        $headers = array_keys($this->headers);

        foreach ($headers as $name) {
            header($this->getRaw($name));
        }

        http_response_code($this->statusCode);

        return $this;
    }

    /**
     * Returns FALSE if no HTTP headers have already been sent or TRUE otherwise.
     *
     * @return bool
     */
    public function hasSent() {
        return headers_sent();
    }

    /**
     * Clears all headers
     *
     * @return $this
     */
    public function clear() {
        $this->headers = array();

        return $this;
    }

    /**
     * Returns a raw HTTP header
     *
     * @param string $name
     *
     * @return string
     */
    public function getRaw($name) {
        $value = $this->get($name); // @todo chunk
        $name  = str_replace(' ', '-', ucwords(str_replace('-', ' ', strtolower($name))));

        return $name . ':' . $value;
    }

    /**
     * Sets HTTP header from a string
     *
     * @param string $value
     *
     * @return Header
     */
    public function setRaw($value) {
        $value = explode(':', $value, 2);

        return $this->set(trim($value[0]), trim($value[1]));
    }

    public function toArray() {
        return $this->headers;
    }

    /**
     * Returns all raw HTTP headers
     *
     * @return string
     */
    public function toString() {
        $raw     = array();
        $headers = array_keys($this->headers);

        foreach ($headers as $name) {
            array_push($raw, $this->getRaw($name));
        }

        return implode(self::EOL, $raw);
    }

    /**
     * Magice function
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }

    /**
     * Initializes the headers
     *
     * @param mixed $headers
     *
     * @return $this
     */
    public function extract($headers = null) {
        if (is_array($headers)) {
            $this->extractFromArray($headers);
        } elseif ('string' === gettype($headers)) {
            $this->extractFromString($headers);
        }

        return $this;
    }

    /**
     * Helper function
     *
     * Initializes the headers from string
     *
     * @param string $headers
     *
     * @return void
     */
    protected function extractFromString($headers) {
        $lines          = explode("\n", trim($headers));
        $lastHeaderName = '';

        foreach ($lines as $line) {
            $line = trim($line, self::EOL);

            if ('' === $line) {
                continue;
            }

            if (preg_match('~^\s~', $line)) {
                $this->set($lastHeaderName, $this->get($lastHeaderName) . ' ' . trim($line));
            } else {
                list($name, $value) = explode(':', $line, 2);
                $lastHeaderName = $name;
                $this->set($name, $value);
            }
        }
    }

    /**
     * Helper function
     *
     * Initializes the headers from array
     *
     * @param array $headers
     *
     * @return void
     */
    protected function extractFromArray(array $headers) {
        foreach ($headers as $name => $value) {
            $name = strtolower(str_replace('_', '-', $name));

            $this->set($name, $value);
        }
    }

    /**
     * Returns parsed header value
     *
     * @param string $headerValue
     *
     * @static
     * @return array
     */
    protected static function parseValue($headerValue) {
        $headerValue = explode(',', $headerValue);
        $return      = array();

        foreach ($headerValue as $part) {
            $mediaType = null;
            $params    = array();
            $subParts  = explode(';', $part);

            foreach ($subParts as $subPart) {
                if (preg_match('~=~', $subPart)) {
                    $subPart                   = explode('=', $subPart, 2);
                    $params[trim($subPart[0])] = preg_replace('~^[\'"]|[\'"]$~', '', $subPart[1]);
                } else {
                    $mediaType = trim($subPart);
                }
            }

            if ($mediaType) {
                $return[$mediaType] = $params;
            }
        }

        return $return;
    }
}
