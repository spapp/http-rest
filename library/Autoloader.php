<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.23.
 */

/**
 * Autoloader implementation.
 *
 * @link http://www.php-fig.org/
 */
class Autoloader {
    /**
     * Php file suffix.
     */
    const PHP_FILE_SUFFIX = '.php';

    /**
     * Class instance.
     *
     * @var null|Autoloader
     * @static
     */
    protected static $instance = null;

    /**
     * Constructor
     */
    protected function __construct() {
    }

    /**
     * Returns Autoloader instance.
     *
     * @static
     * @return Autoloader
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds some include path option.
     *
     * @param string|array $includePath
     *
     * @return $this
     */
    public function addIncludePath($includePath) {
        set_include_path(
                implode(
                        PATH_SEPARATOR,
                        (array)$includePath
                ) . PATH_SEPARATOR . get_include_path()
        );

        return $this;
    }

    /**
     * Register loader with SPL autoloader stack.
     *
     * @return $this
     */
    public function register() {
        spl_autoload_register(
                array(
                        $this,
                        'loadClass'
                )
        );

        return $this;
    }

    /**
     * Unregister this class loader from the SPL autoloader stack.
     *
     * @return $this
     */
    public function unregister() {
        spl_autoload_unregister(
                array(
                        $this,
                        'loadClass'
                )
        );

        return $this;
    }

    /**
     * Loads the given class or interface.
     *
     * @param string $className The name of the class to load.
     *
     * @return $this
     */
    public function loadClass($className) {
        $fileName = str_replace(
                            array(
                                    '\\',
                                    '_'
                            ),
                            DIRECTORY_SEPARATOR,
                            rtrim($className, '\\')
                    ) . self::PHP_FILE_SUFFIX;

        try {
            require_once($fileName);
        } catch (Exception $e) {
            echo $e->getTraceAsString();
        }

        return $this;
    }
}
