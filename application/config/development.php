<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.04.03.
 */

return array(
        'controller' => array(
                'ns'   => ''
        ),

        'content'    => array(
                'templatePath' => APPLICATION_PATH . '/application/view'
        ),

        'router'     => array()
);
