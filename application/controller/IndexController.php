<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.04.03.
 */

use Http\Rest\Controller as RestController;

/**
 * Class IndexController
 */
class IndexController extends RestController {
    public function getAction() {
        $this->getResponse()
             ->addParam('name', 'User')
             ->getContent()
             ->setOption('template', 'index/get')
             ->setType(Http::CONTENT_TYPE_HTML);
    }
}

































































