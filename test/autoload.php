<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.27.
 */

define('APPLICATION_PATH', dirname(__FILE__));

require(dirname(APPLICATION_PATH) . '/library/Autoloader.php');

Autoloader::getInstance()
          ->addIncludePath(
                  array(
                          APPLICATION_PATH . '/library',
                          dirname(APPLICATION_PATH) . '/library'
                  )
          )
          ->register();









































