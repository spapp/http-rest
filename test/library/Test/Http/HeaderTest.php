<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.27.
 */

namespace Test\Http;

use Http;
use Http\Header;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class HeaderTest
 */
class HeaderTest extends TestCase {

    protected $header = array(
            'Accept'            => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding'   => 'gzip, deflate, sdch',
            'Accept-Language'   => 'hu-HU,hu;q=0.8,en-US;q=0.6,en;q=0.4',
            'Cache-Control'     => 'max-age=0',
            'Connection'        => 'keep-alive',
            'Host'              => 'example.com',
            'If-Modified-Since' => 'Fri, 09 Aug 2013 23:54:35 GMT',
            'If-None-Match'     => '"359670651"',
            'User-Agent'        => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/40.0.2214.111 Chrome/40.0.2214.111 Safari/537.36'
    );

    protected $strHeader = <<<HEADER
Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Encoding:gzip, deflate, sdch
Accept-Language:hu-HU,hu;q=0.8,en-US;q=0.6,en;q=0.4
Cache-Control:max-age=0
Connection:keep-alive
Host:example.com
If-Modified-Since:Fri, 09 Aug 2013 23:54:35 GMT
If-None-Match:"359670651"
User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/40.0.2214.111 Chrome/40.0.2214.111 Safari/537.36
HEADER;

    public function testExtractAndToString() {
        $headerFromString = new Header($this->strHeader);
        $headerFromArray  = new Header($this->header);

        $this->assertEquals($headerFromArray->toString(), $headerFromString->toString());
    }

    public function testSetRaw() {
        $header = new Header();

        $header->setRaw(Http::HEADER_ACCEPT . ':' . $this->header[Http::HEADER_ACCEPT]);
        $this->assertEquals($header->get(Http::HEADER_ACCEPT), $this->header[Http::HEADER_ACCEPT]);
    }

    public function testGetRaw() {
        $header = new Header($this->strHeader);

        $this->assertEquals(
                $header->getRaw(Http::HEADER_ACCEPT),
                Http::HEADER_ACCEPT . ':' . $this->header[Http::HEADER_ACCEPT]
        );
    }

    public function testClear() {
        $header = new Header($this->strHeader);

        $this->assertEquals($header->has(Http::HEADER_ACCEPT), true);
        $header->clear();
        $this->assertEquals($header->has(Http::HEADER_ACCEPT), false);
    }

    public function testGet() {
        $header = new Header($this->strHeader);

        $this->assertEquals($header->get(Http::HEADER_ACCEPT), $this->header[Http::HEADER_ACCEPT]);
        $this->assertEquals($header->get(Http::HEADER_ACCEPT_ENCODING), $this->header[Http::HEADER_ACCEPT_ENCODING]);
        $this->assertEquals($header->get(Http::HEADER_ACCEPT_LANGUAGE), $this->header[Http::HEADER_ACCEPT_LANGUAGE]);
    }

    public function testSet() {
        $header = new Header();

        $header->set(Http::HEADER_ACCEPT, $this->header[Http::HEADER_ACCEPT]);

        $this->assertEquals($header->get(Http::HEADER_ACCEPT), $this->header[Http::HEADER_ACCEPT]);
    }

    public function testHas() {
        $header = new Header();

        $this->assertEquals($header->has(Http::HEADER_ACCEPT), false);
        $this->assertEquals($header->get(Http::HEADER_ACCEPT, 'none'), 'none');

        $header->set(Http::HEADER_ACCEPT, $this->header[Http::HEADER_ACCEPT]);

        $this->assertEquals($header->has(Http::HEADER_ACCEPT), true);
        $this->assertEquals($header->get(Http::HEADER_ACCEPT), $this->header[Http::HEADER_ACCEPT]);
    }
}
