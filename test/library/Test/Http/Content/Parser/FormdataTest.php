<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.03.27.
 */

namespace Test\Http\Content\Parser;

use Http;
use Http\Content\Parser\Formdata;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class FormdataTest
 */
class FormdataTest extends TestCase {
    protected $boundary = 'boundary0000008348438548954767865476786';
    protected $params   = array(
            'text'   => 'text default',
            'number' => '11',
            'data'   => array(
                    'a' => 'a1',
                    'b' => 1
            ),
            'file1'  => 'Content of a.txt.',
            'file2'  => "<!DOCTYPE html>\n<html>\n<head>\n<title>a.html</title>\n</head>\n<body>Content of a.html.</body>\n</html>"
    );

    protected $content = <<<CONTENT
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="text"

text default
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="number"

11
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="data"
Content-Type: application/json

{"a":"a1","b":1}
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="file1"; filename="a.txt"
Content-Type: text/plain

Content of a.txt.

--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="file2"; filename="a.html"
Content-Type: text/html

<!DOCTYPE html>
<html>
<head>
<title>a.html</title>
</head>
<body>Content of a.html.</body>
</html>

--boundary0000008348438548954767865476786--
CONTENT;

    public function testStringify() {
        $parser = new Formdata(
                array(
                        'boundary' => $this->boundary
                )
        );

        $content = $parser->stringify($this->params);
        $params  = $parser->parse($content);

        $this->_assert($this->params, $params);
    }

    public function testParse() {
        $parser = new Formdata(
                array(
                        'boundary' => $this->boundary
                )
        );

        $params = $parser->parse($this->content);

        $this->_assert($this->params, $params);
    }

    protected function _assert(array $expected, array $actual) {
        $this->assertArrayHasKey('text', $actual);
        $this->assertArrayHasKey('number', $actual);
        $this->assertArrayHasKey('data', $actual);
        $this->assertArrayHasKey('file1', $actual);
        $this->assertArrayHasKey('file2', $actual);

        $this->assertEquals($expected['text'], $actual['text']);
        $this->assertEquals($expected['number'], $actual['number']);
        $this->assertEquals(true, is_array($actual['data']));
        $this->assertEquals($expected['file1'], $actual['file1']);
        $this->assertEquals($expected['file2'], $actual['file2']);
    }
}
