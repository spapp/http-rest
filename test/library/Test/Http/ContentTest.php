<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Test\Http
 * @since      2015.03.31.
 */

namespace Test\Http;

use Http;
use Http\Content;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class ContentTest
 */
class ContentTest extends TestCase {
    protected $boundary = 'boundary0000008348438548954767865476786';

    protected $formContent = 'a=a1&b=1';
    protected $jsonContent = '{"a":"a1","b":1}';

    protected $textContent = <<<TEXT_CONTENT
<!DOCTYPE html>
<html>
<head>
<title>a.html</title>
</head>
<body>Content of a.html.</body>
</html>
TEXT_CONTENT;

    protected $formdataContent2 = <<<FORMDATA_CONTENT2
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="a"

a1
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="b"

1
--boundary0000008348438548954767865476786--
FORMDATA_CONTENT2;

    protected $formdataContent = <<<FORMDATA_CONTENT
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="text"

text default
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="number"

11
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="data"
Content-Type: application/json

{"a":"a1","b":1}
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="file1"; filename="a.txt"
Content-Type: text/plain

Content of a.txt.

--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="file2"; filename="a.html"
Content-Type: text/html

<!DOCTYPE html>
<html>
<head>
<title>a.html</title>
</head>
<body>Content of a.html.</body>
</html>
--boundary0000008348438548954767865476786--
FORMDATA_CONTENT;

    public function testTextTypeIn() {
        $content = new Content(
                $this->textContent,
                array(
                        'type' => Http::CONTENT_TYPE_TEXT
                )
        );

        $this->assertEquals($this->textContent, $content->toString());
    }

    public function testTextTypeOut() {
        $content = new Content(
                array(
                        'type' => Http::CONTENT_TYPE_TEXT
                )
        );

        $this->_assert($content);

        $this->assertEquals('one value', $content->toString());

        $content->add('two', 2);
        $this->assertEquals('one value' . Http::EOL . '2', $content->toString());
    }

    public function testJsonTypeIn() {
        $content = new Content(
                $this->jsonContent,
                array(
                        'type' => Http::CONTENT_TYPE_JSON
                )
        );

        $this->assertEquals('a1', $content->get('a'));
        $this->assertEquals(1, $content->get('b'));
        $this->assertEquals($this->jsonContent, $content->toString());
    }

    public function testJsonTypeOut() {
        $content = new Content(
                array(
                        'type' => Http::CONTENT_TYPE_JSON
                )
        );

        $this->_assert($content);

        $content->clear();
        $this->assertEquals('[]', $content->toString());

        $json = json_decode($this->jsonContent, true);

        foreach ($json as $k => $v) {
            $content->add($k, $v);
        }

        $this->assertEquals($this->jsonContent, $content->toString());
    }

    public function testFormTypeIn() {
        $content = new Content(
                $this->formContent,
                array(
                        'type' => Http::CONTENT_TYPE_FORM_URLENCODED
                )
        );

        $this->assertEquals('a1', $content->get('a'));
        $this->assertEquals(1, $content->get('b'));
        $this->assertEquals($this->formContent, $content->toString());
    }

    public function testFormTypeOut() {
        $content = new Content(
                array(
                        'type' => Http::CONTENT_TYPE_FORM_URLENCODED
                )
        );

        $this->_assert($content);

        $content->clear();
        $this->assertEquals('', $content->toString());

        $json = json_decode($this->jsonContent, true);

        foreach ($json as $k => $v) {
            $content->add($k, $v);
        }

        $this->assertEquals($this->formContent, $content->toString());
    }

    public function testFormdataTypeIn() {
        $content = new Content(
                $this->formdataContent,
                array(
                        'type'     => Http::CONTENT_TYPE_FORM_DATA,
                        'boundary' => $this->boundary
                )
        );

        $this->assertEquals('text default', $content->get('text'));
        $this->assertEquals(11, $content->get('number'));
    }

    public function testFormdataTypeOut() {
        $content = new Content(
                array(
                        'type'     => Http::CONTENT_TYPE_FORM_DATA,
                        'boundary' => $this->boundary
                )
        );

        $this->_assert($content);

        $content->clear();
        $this->assertEquals('', $content->toString());

        $json = json_decode($this->jsonContent, true);

        foreach ($json as $k => $v) {
            $content->add($k, $v);
        }

        $this->assertEquals(preg_replace('~\n~', Http::EOL, $this->formdataContent2), $content->toString());
    }

    protected function _assert(&$content) {
        $content->add('one', 'one value');
        $this->assertEquals('one value', $content->get('one'));

        $content->add('two', 2);
        $this->assertEquals(2, $content->get('two'));

        $content->remove('two');
        $this->assertEquals(null, $content->get('two'));
    }
}
