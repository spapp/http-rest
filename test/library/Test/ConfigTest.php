<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_rest
 * @subpackage Test
 * @since      2015.04.02.
 */

namespace Test;

use Config;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class ConfigTest
 */
class ConfigTest extends TestCase {
    protected static $resourcesPath;

    public static function setUpBeforeClass() {
        self::$resourcesPath = APPLICATION_PATH . '/resources';
    }

    public function testFactory() {
        $fileName  = self::$resourcesPath . '/config.php';
        $config    = Config::factory($fileName);
        $configInc = include($fileName);

        $this->assertTrue(is_array($config->toArray()));
        $this->assertTrue($config->has('controller'));
        $this->assertTrue(is_array($config->get('controller')->toArray()));
        $this->assertTrue($config->has('content'));
        $this->assertEquals($configInc['content']['templatePath'], $config->get('content')->get('templatePath'));
        $this->assertTrue($config->has('router'));
        $this->assertTrue(is_array($config->get('router')->get('route')->toArray()));
    }
}



























