<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_rest
 * @since     2015.04.02.
 */

/**
 * Application config example
 *
 * @var array
 */
return array(
    /**
     * @property array application controllers options
     */
        'controller'  => array(
                'ns'   => ''
        ),
    /**
     * @property array application response content options
     */
        'content'     => array(
                'templatePath' => ''
        ),
    /**
     * @property array application router options
     */
        'router'      => array(
            /**
             * @property array application routing options
             */
                'route' => array(
                        '~^/one-request~' => array(
                                'controller' => '',
                                'action'     => ''
                        )
                )
        )
);
